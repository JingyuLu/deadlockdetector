package lwh;

public class TableInfo {
    private String fileName;
    private String tableName;
    private String lockType;
    
    public TableInfo(String fileName, String tableName, String lockType){
        this.fileName = fileName;
        this.tableName = tableName;
        this.lockType = lockType;
    }

    public String getFileName() {
        return fileName;
    }

    public String getTableName() {
        return tableName;
    }

    public String getLockType() {
        return lockType;
    }    
}
