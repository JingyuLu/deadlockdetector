package lwh;

import java.io.File;
import java.util.LinkedList;

public class FileList {
	
	public LinkedList<String> readFilesDir(String path) {
		
		LinkedList<File> dirList = new LinkedList<File>();
		LinkedList<String> fileList = new LinkedList<String>();
		File dir = new File(path);
		File[] files = dir.listFiles();

		for (File file : files) {
			if (file.isDirectory()) {
				dirList.add(file);
			} else {
				// process files
				fileList.add(file.getAbsolutePath());
//				//test print
//				System.out.println(file.getAbsolutePath());
			}
		}
		
//		// contain another folder(s)
//		File temp;
//		while (!dirList.isEmpty()) {
//			temp = dirList.removeFirst();
//			if (temp.isDirectory()) {
//				files = temp.listFiles();
//				if (files == null)
//					continue;
//				for (File file : files) {
//					if (file.isDirectory()) {
//						dirList.add(file);
//					} else {
//						fileList.add(file.getAbsolutePath());
//						System.out.println(file.getAbsolutePath());
//					}
//				}
//			} else {
//				System.out.println("-------------");
//			}
//		}
		return fileList;
	}
}
