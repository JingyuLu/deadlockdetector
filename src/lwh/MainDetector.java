package lwh;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class MainDetector {

	private static File path;
	private static FileList fl=new FileList();
	private static SQLContent sqlCon=new SQLContent();
	static ArrayList<ArrayList<TableInfo>> transOnTable = new ArrayList<>();

	public static void main(String[] args) {

		String SQLDIR = "D:\\ѧҵ\\MyConcordia\\2018-fall\\SOEN7481\\Project\\data\\ArtificialSystem\\ArtificialSystem\\Backup\\DAN\\AllTransactions";
//		String SQLDIR = "/Users/win10/Desktop/SE-Software Engineering/7481soen/Project/"
//                        + "project/stepdad/ControllerSystem/Backup/HIM/AllTransactions";
		int len = SQLDIR.length();

		LinkedList<String> sqlFileList=fl.readFilesDir(SQLDIR);  
		for(String tempDir : sqlFileList) {
			tableMaker(len,tempDir);
		}
                for(int i = 0; i < transOnTable.size(); i++){
                    ArrayList<TableInfo> list1 = transOnTable.get(i);
                    for(int j = i + 1; j < transOnTable.size(); j++){
                        ArrayList<TableInfo> list2 = transOnTable.get(j);
                        for(int x = 0; x < list1.size(); x++){
                            for(int y = 0; y < list2.size(); y++){
                                if(x < list2.size() && y < list1.size()){
                                    if(list1.get(x).getTableName().equals(list2.get(y).getTableName())
                                            && list1.get(y).getTableName().equals(list2.get(x).getTableName())
                                            && !list1.get(x).getTableName().equals(list1.get(y).getTableName())
                                            && x <= y
                                            && !(list1.get(x).getLockType().equals("shared") && list2.get(y).getFileName().equals("shared"))){
                                        System.out.println("There is a deadlock in these two files: " + list1.get(x).getFileName()
                                         + " " + list2.get(y).getFileName());
                                    }
                                }
                            }
                        }
                    }
                }
	}

        private static void tableMaker(int len, String tempDir) {
            path = new File(tempDir);
            String fileName = tempDir.substring(len,tempDir.length() - 4);
            String[] sqlStmt = sqlCon.getSQLStatements(path).toLowerCase().replaceAll("[~!@#$%^&*()+=|{}':;'<>/?~！@#]", " ").
                        replaceAll("\\s{1,}", " ").trim().split(" ");
            
            ArrayList<TableInfo> table = new ArrayList<>();
            for(int i = 0; i < sqlStmt.length; i++){
                if(sqlStmt[i].equals("update") || sqlStmt[i].equals("insert") || sqlStmt[i].equals("delete")){
                    if(sqlStmt[i + 1].contains(".")){
                        String[] tableName = sqlStmt[i + 1].replace(".", " ").split(" ");
                        String Name = tableName[1];
                        TableInfo curTable = new TableInfo(fileName, Name, "exclusive");
                        table.add(curTable);
//                        System.out.println(Name + " " + fileName);
                    }else{
                        TableInfo curTable = new TableInfo(fileName, sqlStmt[i + 1], "exclusive");
                        table.add(curTable);
//                        System.out.println(sqlStmt[i + 1] + " " + fileName);
                    }                    
                }else if(sqlStmt[i].equals("from")){
                    if(sqlStmt[i + 1].contains(".")){
                        String[] tableName = sqlStmt[i + 1].replace(".", " ").split(" ");
                        String Name = tableName[1];
                        TableInfo curTable = new TableInfo(fileName, Name, "shared");
                        table.add(curTable);
//                        System.out.println(Name + " " + fileName);
                    }else{
                        TableInfo curTable = new TableInfo(fileName, sqlStmt[i + 1], "shared");
                        table.add(curTable);
//                        System.out.println(sqlStmt[i + 1] + " " + fileName);
                    }
                }
            }
            transOnTable.add(table);
//            for(int i = 0; i < table.size(); i++){
//                System.out.println("fileName: " + fileName + "; tableName: " + table.get(i).getTableName() +
//                        "; lockType: " + table.get(i).getLockType() + "; order: " + i);
//            }
//            System.out.println("---------------------");
	}
                
//	private static void tableMaker(int len, String tempDir) {
//		
//		path=new File(tempDir);
//		String[] sqlStmt=sqlCon.getSQLStatements(path).toLowerCase().split(" ");
//
//		if(sqlStmt[0].trim().equals("select")) {
//			String curTable=sqlStmt[3];
//			if(!transOnTable.containsKey(curTable)) {
//				transOnTable.put(sqlStmt[3], "select"+" "+tempDir.substring(len+1,tempDir.length()-4));
//			}else if(transOnTable.get(curTable).split(" ")[0].equals("select")) {
//				System.out.println("Shared Lock, multiple accesses are allowed!");
//			}else {
//				System.out.println("DeadLock detected: "
//						+transOnTable.get(curTable).split(" ")[1]+" "
//						+tempDir.substring(len+1,tempDir.length()-4));
//			}
//		}else {
//			String curTable=sqlStmt[1];
//			if(!transOnTable.containsKey(curTable)) {
//				transOnTable.put(curTable, sqlStmt[0]+" "+tempDir.substring(len+1,tempDir.length()-4));
//			}else {
//				System.out.println("DeadLock detected: "
//						+transOnTable.get(curTable).split(" ")[1]+" "
//						+tempDir.substring(len+1,tempDir.length()-4));
//			}
//		}
//	}
	 
}
