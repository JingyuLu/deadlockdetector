package lwh;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class SQLContent {

	public String getSQLStatements(File file) {

		StringBuilder result = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String s = null;
			while ((s = br.readLine()) != null) {// readLine,one row per time
				result.append(System.lineSeparator() + s);
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result.toString();
	}
}
